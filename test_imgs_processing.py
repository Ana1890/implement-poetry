import pandas as pd
import tensorflow as tf
import cv2
import numpy as np
import imgaug.augmenters as iaa

from constants import IMAGE_DIR

print("Esta es mi imagen original: ")
img = cv2.imread(IMAGE_DIR + "scientist.jpg")
print("tamaño: ", img.shape)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

hist, bins = np.histogram(img[:, :, 0].flatten(), 256, [0, 256])
cdf = hist.cumsum()
cdf_normalized = cdf * float(hist.max()) / cdf.max()

cdf_m = np.ma.masked_equal(cdf, 0)
cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
cdf = np.ma.filled(cdf_m, 0).astype("uint8")

img2 = cdf[img]
img2 = cv2.cvtColor(img2, cv2.COLOR_RGB2BGR)

print("Esta es la imagen que voy a guardar")
cv2.imwrite(IMAGE_DIR + "scientist2.jpg", img2)
